#ifndef ENVIRONMENT_GENERATOR_H
#define ENVIRONMENT_GENERATOR_H

#include <ros/ros.h>
#include <visualization_msgs/Marker.h>
#include <visualization_msgs/MarkerArray.h>
#include <std_srvs/Empty.h>


enum LogLevel
{
  INFO, WARN, ERROR
};


class EnvironmentGenerator
{
private:

  // TODO hacking
  int object1_shape_, object2_shape_, object3_shape_;
  std::string reference_frame_, obj_state_topic_;
  std::vector<double> object1_pose_, object2_pose_, object3_pose_;
  std::vector<double> object1_dims_, object2_dims_, object3_dims_;
  std::vector<double> object1_color_, object2_color_, object3_color_;

  // ROS
  ros::NodeHandle nh_;
  ros::Rate rate_;
  ros::Publisher marker_pub_;
  ros::ServiceServer reset_srv_;

  // TODO temporary until multi object figured out
  visualization_msgs::Marker object1_marker_, object2_marker_, object3_marker_; 
  visualization_msgs::MarkerArray marker_array_;

  bool resetEnvironment(std_srvs::Empty::Request &req, std_srvs::Empty::Response &resp);
  void log(std::string msg, LogLevel level);

public:
EnvironmentGenerator(ros::NodeHandle &nh) : rate_(1.0), nh_(nh) {} // temporary for rate

  bool init();
  void run();
};



#endif

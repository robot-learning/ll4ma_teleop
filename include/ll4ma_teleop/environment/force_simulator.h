#ifndef FORCE_SIMULATOR_H
#define FORCE_SIMULATOR_H

#include <ros/ros.h>
#include <visualization_msgs/MarkerArray.h>
#include <geometry_msgs/Pose.h>

/* #include <Eigen/Geometry> */

//#include "fcl/math/constants.h"
//#include "fcl/narrowphase/collision.h"
#include "fcl/narrowphase/detail/gjk_solver_libccd.h"

#include <phantom_omni/OmniFeedback.h>
#include "ll4ma_teleop/ControlBoardState.h"
#include <ll4ma_robot_control_msgs/RobotState.h>


enum LogLevel
{
  INFO, WARN, ERROR
};


class ForceSimulator
{
  typedef fcl::Box<double> Boxd;
  typedef fcl::Sphere<double> Sphered;
  typedef std::map<int, fcl::Transform3<double>> TransformMap;
  typedef std::map<int, Boxd> BoxMap; // TEMPORARY how to make arbitrary shape? ShapeBase?

private:

  std::string obj_state_topic_, robot_state_topic_, omni_fb_topic_, omni_cb_topic_;
  double ee_radius_, run_rate_, pd_, max_pd_, force_, max_force_;
  bool force_fb_active_;
  // TODO assuming ee approximated by sphere for now
  
  // FCL
  Sphered ee_; // TODO assuming for now end-effector approximated by sphere
  fcl::Transform3d tf_ee_;
  fcl::detail::GJKSolver_libccd<double> solver_;
  fcl::Quaterniond q_from_msg_, q_to_msg_, q_ee_;  // for conversions ROS <-> FCL
  fcl::Vector3d t_from_msg_, t_to_msg_, t_ee_; // for conversions ROS <-> FCL
  std::vector<fcl::ContactPoint<double>> contacts_;

  TransformMap tf_map_;
  BoxMap shape_map_;
  
  // ROS
  ros::NodeHandle nh_;
  ros::Rate rate_;
  ros::Subscriber obj_state_sub_, robot_state_sub_, omni_cb_sub_;
  ros::Publisher omni_fb_pub_;
  phantom_omni::OmniFeedback omni_fb_;

  void log(std::string msg, LogLevel level);
  void objStateCallback(visualization_msgs::MarkerArray marker_array);
  void robotStateCallback(ll4ma_robot_control_msgs::RobotState state);
  void omniControlBoardStateCallback(ll4ma_teleop::ControlBoardState state);

  template <typename S1, typename S2>
  double getPenetrationDepth(const S1 &s1, const fcl::Transform3<double> &tf1,
                             const S2 &s2, const fcl::Transform3<double> &tf2,
                             std::vector<fcl::ContactPoint<double>> &contacts)
  {
    bool is_intersecting = solver_.shapeIntersect(s1, tf1, s2, tf2, &contacts);
    return (is_intersecting) ? contacts.back().penetration_depth : 0.0;
  }

public:
ForceSimulator(std::string ns) : rate_(1.0), ee_(1.0), nh_(ns) {} // temporary for rate and sphere

  bool init();
  void run();
};



#endif

#ifndef LL4MA_TELEOP_RVIZ_PLUGIN_TELEOP_PANEL_H_
#define LL4MA_TELEOP_RVIZ_PLUGIN_TELEOP_PANEL_H_

#include <ros/ros.h>
#include <rviz/panel.h>
#include <QToolButton>

namespace constants
{
const int ICON_WIDTH = 150;
const int ICON_HEIGHT = 100;
const std::string DEFAULT_BUTTON_FORMAT =
    "QToolButton {"
    "background-color: #756563;"
    "color: #FFFFFF;"
    "font-size : 14px;}"
    "QToolButton:hover {"
    "background-color: #CEB8B5;"
    "color: #FFFFFF;"
    "font-size : 14px;}";
const std::string ACTIVE_BUTTON_FORMAT =
    "QToolButton {"
    "background-color: #197C3E;"
    "color: #FFFFFF;"
    "font-size : 14px;}"
    "QToolButton:hover {"
    "background-color: #80CB9C;"
    "color: #FFFFFF;"
    "font-size : 14px;}";
} // namespace constants

class TeleopPanel : public rviz::Panel
{
  Q_OBJECT
public:
  TeleopPanel(QWidget *parent = 0);

  virtual void load(const rviz::Config &config);
  virtual void save(rviz::Config config) const;

public Q_SLOTS:

  void resetEnvironment();
  void handlePosition();
  void handleOrientation();
  void handleForceFeedback();

protected Q_SLOTS:

protected:
  ros::NodeHandle nh_;
  ros::ServiceClient reset_dart_env_client_;
  ros::ServiceClient reset_commander_client_;
  ros::ServiceClient enable_force_client_;
  ros::ServiceClient enable_pos_client_;
  ros::ServiceClient enable_orient_client_;

  bool force_fb_active_;
  bool position_active_;
  bool orientation_active_;

  QToolButton *reset_env_btn_;
  QToolButton *force_fb_btn_;
  QToolButton *position_btn_;
  QToolButton *orientation_btn_;
};

#endif
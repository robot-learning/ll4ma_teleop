#ifndef ROBOT_JOINT_COMMANDER
#define ROBOT_JOINT_COMMANDER

// ROS
#include <ros/ros.h>
#include <sensor_msgs/JointState.h>
#include <geometry_msgs/Pose.h>
#include <geometry_msgs/Quaternion.h>
#include <std_srvs/Trigger.h>
#include <ll4ma_teleop/TeleopCommand.h>

// KDL
#include <kdl_parser/kdl_parser.hpp>
#include <kdl/jntarrayvel.hpp>
#include <kdl/chainfksolvervel_recursive.hpp>
#include <kdl/chainjnttojacsolver.hpp>

// boost
#include <boost/scoped_ptr.hpp>

// Eigen
#include <Eigen/Dense>

class RobotJointCommander
{
private:
  // std
  int num_jnts_;
  double dt_;
  bool use_current_joint_state_, have_cmd_;
  std::string root_link_, tip_link_, robot_description_;
  std::string jnt_cmd_topic_, jnt_state_topic_, teleop_cmd_topic_;
  std::vector<std::string> jnt_names_;
  std::vector<double> jnt_upper_lims_, jnt_lower_lims_, nominal_jnts_;

  // KDL
  KDL::Tree robot_tree_;
  KDL::Chain kdl_chain_;
  KDL::Jacobian J_kdl_;
  KDL::JntArrayVel q_qdot_;
  KDL::JntArray prev_q_;
  KDL::Frame x_, prev_x_;
  KDL::FrameVel x_xdot_;
  KDL::Twist x_diff_kdl_;
  boost::scoped_ptr<KDL::ChainJntToJacSolver> jac_solver_;
  boost::scoped_ptr<KDL::ChainFkSolverVel> fk_solver_;

  // Eigen
  Eigen::VectorXd x_diff_, q_diff_, factor_;
  Eigen::MatrixXd J_, J_inv_;

  // ROS
  ros::Rate rate_;
  ros::NodeHandle nh_;
  ros::Publisher jnt_cmd_pub_;
  ros::Subscriber teleop_cmd_sub_, jnt_state_sub_;
  ros::ServiceServer reset_srv_, set_nominal_srv_;
  sensor_msgs::JointState jnt_state_, jnt_cmd_;

  bool resetRobotState(std_srvs::Trigger::Request &req, std_srvs::Trigger::Response &resp);
  bool setNominalRobotState(std_srvs::Trigger::Request &req, std_srvs::Trigger::Response &resp);
  void teleopCommandCallback(ll4ma_teleop::TeleopCommand cmd);
  void getPseudoInverse(Eigen::MatrixXd &m, Eigen::MatrixXd &m_pinv, double tolerance);
  void initJointCommand();
  bool controlEngaged();
  void jointStateCallback(sensor_msgs::JointState msg);

public:
 RobotJointCommander(double rate, ros::NodeHandle nh) : rate_(rate), nh_(nh), dt_(1./rate) {}

  bool init();
  void run();
};

#endif

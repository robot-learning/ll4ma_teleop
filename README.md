# LL4MA Teleoperation

This package is for teleoperation of simulated and real robots. Any device drivers for input devices are in their own packages (e.g. the Phantom Omni driver is in [phatom_omni](https://bitbucket.org/robot-learning/phantom_omni)). 

## Installation

You only need to clone this package into a catkin workspace and follow the usual build procedures. This package does have the following dependencies that you'll need:

  * [phantom_omni](https://bitbucket.org/robot-learning/phantom_omni): device driver for the Phantom Omni
  * [ll4ma_robots_description](https://bitbucket.org/robot-learning/ll4ma_robots_description): description package housing URDF and STL files for visualizing and controlling robots.
  * [ll4ma_msgs](https://bitbucket.org/robot-learning/ll4ma_msgs): package only for system-wide messages and service definitions.
  
## Usage

**TODO** the launch files need to be updated with the current setup, and the dependencies for this package and dart_ros are actually backwards. This will be fixed soon (Adam 4/4/2019).

## Troubleshooting
If you're having trouble with the Omni device, see our [driver repo](https://bitbucket.org/robot-learning/phantom_omni) and our [wiki page](https://robot-learning.cs.utah.edu/phantom_omni) for more detailed information. You can test that the device is running using 

    roslaunch phantom_omni omni.launch

which should bring up a model of the Omni in rViz that should render the state of the actual device. If you're seeing an error that the haptic device cannot be initialized, try recompiling the driver:

    rosrun phantom_omni initialize_device.sh -c

Sometimes, when a kernel update has been issued, you might have to go into the source folder for the low-level driver and clean it. To do this, navigate to `phantom_omni/src/dummyraw1394` and issue a `make clean` command. Then try the `rosrun` command above again. 
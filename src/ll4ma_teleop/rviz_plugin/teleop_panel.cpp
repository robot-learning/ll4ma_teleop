#include <ll4ma_teleop/rviz_plugin/teleop_panel.h>

#include <QPainter>
#include <QLineEdit>
#include <QPushButton>
#include <QVBoxLayout>
#include <QLabel>
#include <QTimer>

#include <std_srvs/Trigger.h>
#include <std_srvs/SetBool.h>

#include <pluginlib/class_list_macros.h>

TeleopPanel::TeleopPanel(QWidget *parent) : rviz::Panel(parent)
{
    force_fb_active_ = false;
    position_active_ = false;
    orientation_active_ = false;

    QVBoxLayout *layout = new QVBoxLayout;

    // Reset simulation environment
    reset_env_btn_ = new QToolButton(this);
    reset_env_btn_->setText("Reset Environment");
    reset_env_btn_->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
    reset_env_btn_->setIconSize(QSize(constants::ICON_WIDTH, constants::ICON_HEIGHT));
    reset_env_btn_->setIcon(QIcon(QPixmap(QString("/home/adam/ros_ws/src/ll4ma_teleop/imgs/reset.png"))));
    reset_env_btn_->setStyleSheet(constants::DEFAULT_BUTTON_FORMAT.c_str());
    layout->addWidget(reset_env_btn_);
    connect(reset_env_btn_, SIGNAL(clicked()), this, SLOT(resetEnvironment()));

    position_btn_ = new QToolButton(this);
    position_btn_->setText("Position");
    position_btn_->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
    position_btn_->setIconSize(QSize(constants::ICON_WIDTH, constants::ICON_HEIGHT));
    position_btn_->setIcon(QIcon(QPixmap(QString("/home/adam/ros_ws/src/ll4ma_teleop/imgs/position.png"))));
    position_btn_->setStyleSheet(constants::DEFAULT_BUTTON_FORMAT.c_str());
    layout->addWidget(position_btn_);
    connect(position_btn_, SIGNAL(clicked()), this, SLOT(handlePosition()));

    orientation_btn_ = new QToolButton(this);
    orientation_btn_->setText("Orientation");
    orientation_btn_->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
    orientation_btn_->setIconSize(QSize(constants::ICON_WIDTH, constants::ICON_HEIGHT));
    orientation_btn_->setIcon(QIcon(QPixmap(QString("/home/adam/ros_ws/src/ll4ma_teleop/imgs/orientation.png"))));
    orientation_btn_->setStyleSheet(constants::DEFAULT_BUTTON_FORMAT.c_str());
    layout->addWidget(orientation_btn_);
    connect(orientation_btn_, SIGNAL(clicked()), this, SLOT(handleOrientation()));

    force_fb_btn_ = new QToolButton(this);
    force_fb_btn_->setText("Force Feedback");
    force_fb_btn_->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
    force_fb_btn_->setIconSize(QSize(constants::ICON_WIDTH, constants::ICON_HEIGHT));
    force_fb_btn_->setIcon(QIcon(QPixmap(QString("/home/adam/ros_ws/src/ll4ma_teleop/imgs/haptic.png"))));
    force_fb_btn_->setStyleSheet(constants::DEFAULT_BUTTON_FORMAT.c_str());
    layout->addWidget(force_fb_btn_);
    connect(force_fb_btn_, SIGNAL(clicked()), this, SLOT(handleForceFeedback()));

    setLayout(layout);

    reset_dart_env_client_ = nh_.serviceClient<std_srvs::Trigger>("/dart_environment/reset_environment");
    reset_commander_client_ = nh_.serviceClient<std_srvs::Trigger>("/robot_commander/reset_robot_state");
    enable_force_client_ = nh_.serviceClient<std_srvs::SetBool>("/omni_commander/enable_force_feedback");
    enable_pos_client_ = nh_.serviceClient<std_srvs::SetBool>("/omni_commander/enable_position");
    enable_orient_client_ = nh_.serviceClient<std_srvs::SetBool>("/omni_commander/enable_orientation");
}

void TeleopPanel::resetEnvironment()
{
    std_srvs::Trigger srv;
    // This order of calls matters, if reversed the robot moves more when reset
    reset_commander_client_.call(srv);
    reset_dart_env_client_.call(srv);
}

void TeleopPanel::handlePosition()
{
    std_srvs::SetBool srv;
    if (position_active_)
    {
        srv.request.data = false;
        enable_pos_client_.call(srv);
        position_btn_->setStyleSheet(constants::DEFAULT_BUTTON_FORMAT.c_str());
        position_active_ = false;
    }
    else
    {
        srv.request.data = true;
        enable_pos_client_.call(srv);
        position_btn_->setStyleSheet(constants::ACTIVE_BUTTON_FORMAT.c_str());
        position_active_ = true;
    }
}

void TeleopPanel::handleOrientation()
{
    std_srvs::SetBool srv;
    if (orientation_active_)
    {
        srv.request.data = false;
        enable_orient_client_.call(srv);
        orientation_btn_->setStyleSheet(constants::DEFAULT_BUTTON_FORMAT.c_str());
        orientation_active_ = false;
    }
    else
    {
        srv.request.data = true;
        enable_orient_client_.call(srv);
        orientation_btn_->setStyleSheet(constants::ACTIVE_BUTTON_FORMAT.c_str());
        orientation_active_ = true;
    }
}

void TeleopPanel::handleForceFeedback()
{
    std_srvs::SetBool srv;
    if (force_fb_active_)
    {
        srv.request.data = false;
        enable_force_client_.call(srv);
        force_fb_btn_->setStyleSheet(constants::DEFAULT_BUTTON_FORMAT.c_str());
        force_fb_active_ = false;
    }
    else
    {
        srv.request.data = true;
        enable_force_client_.call(srv);
        force_fb_btn_->setStyleSheet(constants::ACTIVE_BUTTON_FORMAT.c_str());
        force_fb_active_ = true;
    }
}

void TeleopPanel::save(rviz::Config config) const
{
    rviz::Panel::save(config);
}

void TeleopPanel::load(const rviz::Config &config)
{
    rviz::Panel::load(config);
}

PLUGINLIB_EXPORT_CLASS(TeleopPanel, rviz::Panel)

#include "force_simulator.h"
#include <algorithm>    // std::min


bool ForceSimulator::init()
{
  log("Initializing...", INFO);
  
  bool success = true;
  success &= nh_.getParam("obj_state_topic", obj_state_topic_);
  success &= nh_.getParam("robot_state_topic", robot_state_topic_);
  success &= nh_.getParam("omni_fb_topic", omni_fb_topic_);
  success &= nh_.getParam("omni_cb_topic", omni_cb_topic_);
  success &= nh_.getParam("ee_radius", ee_radius_);
  success &= nh_.getParam("state_rate", run_rate_);
  success &= nh_.getParam("max_force", max_force_);
  success &= nh_.getParam("max_pd", max_pd_);
  
  if (!success)
  {
    log("Failed to load params from param server.", ERROR);
    return false;
  }
  else
  {
    log("Successfully loaded params from param server.", INFO);
  }

  obj_state_sub_ = nh_.subscribe(obj_state_topic_, 1, &ForceSimulator::objStateCallback, this);
  robot_state_sub_ = nh_.subscribe(robot_state_topic_, 1, &ForceSimulator::robotStateCallback, this);
  omni_cb_sub_ = nh_.subscribe(omni_cb_topic_, 1, &ForceSimulator::omniControlBoardStateCallback,
                               this);
  omni_fb_pub_ = nh_.advertise<phantom_omni::OmniFeedback>(omni_fb_topic_, 1);

  ee_ = fcl::Sphere<double>(ee_radius_);
  rate_ = ros::Rate(run_rate_);

  force_fb_active_ = false;
  
  log("Initialization complete.", INFO);
  return true;
}


void ForceSimulator::run()
{
  log("Running...", INFO);
  
  while (ros::ok())
  {
    if (!tf_map_.empty())
    {
      // compute penetration depth
      // TODO for now assuming there is only one object
      contacts_.clear(); // IMPORTANT! otherwise you accumulate contacts forever
      pd_ = getPenetrationDepth<Sphered, Boxd>(ee_, tf_ee_, shape_map_[0], tf_map_[0], contacts_);
      // compute force to render on haptic device
      force_ = std::min(pd_ * (max_force_ / max_pd_), max_force_);
      // ROS_INFO_STREAM("PD: " << pd_ << ", FORCE: " << force_);
      // publish the forces to haptic device
      omni_fb_.force.z = force_;
      if (force_fb_active_)
        omni_fb_pub_.publish(omni_fb_);
    }
    ros::spinOnce();
    rate_.sleep();
  }

  log("Complete. Exiting.", INFO);
}

  
void ForceSimulator::objStateCallback(visualization_msgs::MarkerArray marker_array)
{
  if (marker_array.markers.size())
  {
    for (auto const &marker : marker_array.markers)
    {
      // TODO assume for now object dims are set at initial occurrence and never change
      if (!shape_map_.count(marker.id))
      {
        shape_map_[marker.id] = Boxd(marker.scale.x, marker.scale.y, marker.scale.z);
      }
      
      // update object pose
      t_from_msg_[0] = marker.pose.position.x;
      t_from_msg_[1] = marker.pose.position.y;
      t_from_msg_[2] = marker.pose.position.z;
      q_from_msg_.x() = marker.pose.orientation.x;
      q_from_msg_.y() = marker.pose.orientation.y;
      q_from_msg_.z() = marker.pose.orientation.z;
      q_from_msg_.w() = marker.pose.orientation.w;
      tf_map_[marker.id] = fcl::Transform3<double>(q_from_msg_);
      tf_map_[marker.id].translation() = t_from_msg_;
    } 
  }
}


void ForceSimulator::robotStateCallback(ll4ma_robot_control_msgs::RobotState state)
{
  t_ee_[0] = state.pose.position.x;
  t_ee_[1] = state.pose.position.y;
  t_ee_[2] = state.pose.position.z;
  q_ee_.x() = state.pose.orientation.x;
  q_ee_.y() = state.pose.orientation.y;
  q_ee_.z() = state.pose.orientation.z;
  q_ee_.w() = state.pose.orientation.w;
  tf_ee_ = fcl::Transform3<double>(q_ee_);
  tf_ee_.translation() = t_ee_;
}


void ForceSimulator::omniControlBoardStateCallback(ll4ma_teleop::ControlBoardState state)
{
  force_fb_active_ = state.force_fb_active;
}


void ForceSimulator::log(std::string msg, LogLevel level)
{
  switch(level)
  {
    case WARN :
    {
      ROS_WARN_STREAM("[ForceSimulator] " << msg);
      break;
    }
    case ERROR :
    {
      ROS_ERROR_STREAM("[ForceSimulator] " << msg);
      break;    
    }
    default:
    {
      ROS_INFO_STREAM("[ForceSimulator] " << msg);
      break;    
    }
  }
}




int main(int argc, char** argv)
{
  ros::init(argc, argv, "force_simulator");
  ForceSimulator simulator("teleop");
  if (simulator.init())
    simulator.run();
  else
    ROS_ERROR("Bad things happened while intializing ForceSimulator.");
}

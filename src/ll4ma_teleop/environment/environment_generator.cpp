#include "environment_generator.h"


bool EnvironmentGenerator::init()
{
  log("Initializing...", INFO);
  // TODO for now writing this for one object. For multiple objects it will probably be better
  // to not use the parameter server and use a YAML parser instead. For now this is easier.
  
  bool success = true;
  //success &= nh_.getParam("/root_link", reference_frame_);
  success &= nh_.getParam("obj_state_topic", obj_state_topic_);
  success &= nh_.getParam("object_shape", object1_shape_);
  success &= nh_.getParam("object_pose", object1_pose_);
  success &= nh_.getParam("object_dims", object1_dims_);
  success &= nh_.getParam("object_color", object1_color_);

  // TODO hacking
  success &= nh_.getParam("sphere1_shape", object2_shape_);
  success &= nh_.getParam("sphere1_pose", object2_pose_);
  success &= nh_.getParam("sphere1_dims", object2_dims_);
  success &= nh_.getParam("sphere1_color", object2_color_);
  success &= nh_.getParam("sphere2_shape", object3_shape_);
  success &= nh_.getParam("sphere2_pose", object3_pose_);
  success &= nh_.getParam("sphere2_dims", object3_dims_);
  success &= nh_.getParam("sphere2_color", object3_color_);
    
  if (!success)
  {
    log("Failed to load params from param server.", ERROR);
    return false;
  }
  else
  {
    log("Successfully loaded params from param server.", INFO);
  }

  marker_pub_ = nh_.advertise<visualization_msgs::MarkerArray>(obj_state_topic_, 1);

  reset_srv_ = nh_.advertiseService("/environment/reset",
                                    &EnvironmentGenerator::resetEnvironment, this);

  // initialize marker
  object1_marker_.id = 1;
  object1_marker_.header.frame_id = "world"; // reference_frame_;
  object1_marker_.type = object1_shape_;
  object1_marker_.action = visualization_msgs::Marker::ADD;
  object1_marker_.lifetime = ros::Duration();
  object1_marker_.color.r = object1_color_[0];
  object1_marker_.color.g = object1_color_[1];
  object1_marker_.color.b = object1_color_[2];
  object1_marker_.color.a = object1_color_[3];
  object1_marker_.scale.x = object1_dims_[0];
  object1_marker_.scale.y = object1_dims_[1];
  object1_marker_.scale.z = object1_dims_[2];
  object1_marker_.pose.position.x = object1_pose_[0];
  object1_marker_.pose.position.y = object1_pose_[1];
  object1_marker_.pose.position.z = object1_pose_[2];
  object1_marker_.pose.orientation.x = object1_pose_[3];
  object1_marker_.pose.orientation.y = object1_pose_[4];
  object1_marker_.pose.orientation.z = object1_pose_[5];
  object1_marker_.pose.orientation.w = object1_pose_[6];


  // TODO hacking
  object2_marker_.id = 2;
  object2_marker_.header.frame_id = "world"; // reference_frame_;
  object2_marker_.type = object2_shape_;
  object2_marker_.action = visualization_msgs::Marker::ADD;
  object2_marker_.lifetime = ros::Duration();
  object2_marker_.color.r = object2_color_[0];
  object2_marker_.color.g = object2_color_[1];
  object2_marker_.color.b = object2_color_[2];
  object2_marker_.color.a = object2_color_[3];
  object2_marker_.scale.x = object2_dims_[0];
  object2_marker_.scale.y = object2_dims_[1];
  object2_marker_.scale.z = object2_dims_[2];
  object2_marker_.pose.position.x = object2_pose_[0];
  object2_marker_.pose.position.y = object2_pose_[1];
  object2_marker_.pose.position.z = object2_pose_[2];
  object2_marker_.pose.orientation.x = object2_pose_[3];
  object2_marker_.pose.orientation.y = object2_pose_[4];
  object2_marker_.pose.orientation.z = object2_pose_[5];
  object2_marker_.pose.orientation.w = object2_pose_[6];
  
  object3_marker_.id = 3;
  object3_marker_.header.frame_id = "world"; // reference_frame_;
  object3_marker_.type = object3_shape_;
  object3_marker_.action = visualization_msgs::Marker::ADD;
  object3_marker_.lifetime = ros::Duration();
  object3_marker_.color.r = object3_color_[0];
  object3_marker_.color.g = object3_color_[1];
  object3_marker_.color.b = object3_color_[2];
  object3_marker_.color.a = object3_color_[3];
  object3_marker_.scale.x = object3_dims_[0];
  object3_marker_.scale.y = object3_dims_[1];
  object3_marker_.scale.z = object3_dims_[2];
  object3_marker_.pose.position.x = object3_pose_[0];
  object3_marker_.pose.position.y = object3_pose_[1];
  object3_marker_.pose.position.z = object3_pose_[2];
  object3_marker_.pose.orientation.x = object3_pose_[3];
  object3_marker_.pose.orientation.y = object3_pose_[4];
  object3_marker_.pose.orientation.z = object3_pose_[5];
  object3_marker_.pose.orientation.w = object3_pose_[6];
  
  marker_array_.markers.push_back(object1_marker_);
  marker_array_.markers.push_back(object2_marker_);
  marker_array_.markers.push_back(object3_marker_);

  log("Initialization complete.", INFO);
  return true;
}


void EnvironmentGenerator::run()
{
  log("Publishing marker...", INFO);
  while (ros::ok())
  {
    //marker_array_.header.stamp = ros::Time::now();
    marker_pub_.publish(marker_array_);
    ros::spinOnce();
    rate_.sleep();
  }
  log("Complete. Exiting.", INFO);
}


bool EnvironmentGenerator::resetEnvironment(std_srvs::Empty::Request &req,
                                            std_srvs::Empty::Response &resp)
{
  log("Resetting environment...", INFO);
  
  bool success = true;
  //success &= nh_.getParam("/root_link", reference_frame_);
  // success &= nh_.getParam("object_shape", object_shape_);
  // success &= nh_.getParam("object_pose", object_pose_);
  // success &= nh_.getParam("object_dims", object_dims_);
  // success &= nh_.getParam("object_color", object_color_);
  if (!success)
  {
    log("Failed to load params from param server.", ERROR);
    return false;
  }
  else
  {
    log("Successfully loaded params from param server.", INFO);
  }

  // marker_.type = object_shape_;
  // marker_.color.r = object_color_[0];
  // marker_.color.g = object_color_[1];
  // marker_.color.b = object_color_[2];
  // marker_.color.a = object_color_[3];
  // marker_.scale.x = object_dims_[0];
  // marker_.scale.y = object_dims_[1];
  // marker_.scale.z = object_dims_[2];
  // marker_.pose.position.x = object_pose_[0];
  // marker_.pose.position.y = object_pose_[1];
  // marker_.pose.position.z = object_pose_[2];
  // marker_.pose.orientation.x = object_pose_[3];
  // marker_.pose.orientation.y = object_pose_[4];
  // marker_.pose.orientation.z = object_pose_[5];
  // marker_.pose.orientation.w = object_pose_[6];

  // marker_array_.markers[0] = marker_;

  log("Reset!", INFO);
  
  return true;
}


void EnvironmentGenerator::log(std::string msg, LogLevel level)
{
  switch(level)
  {
    case WARN :
    {
      ROS_WARN_STREAM("[EnvironmentGenerator] " << msg);
      break;
    }
    case ERROR :
    {
      ROS_ERROR_STREAM("[EnvironmentGenerator] " << msg);
      break;    
    }
    default:
    {
      ROS_INFO_STREAM("[EnvironmentGenerator] " << msg);
      break;    
    }
  }
}




int main(int argc, char** argv)
{
  ros::init(argc, argv, "environment_generator");
  ros::NodeHandle nh("environment");
  EnvironmentGenerator generator(nh);
  if (generator.init())
    generator.run();
  else
    ROS_ERROR("Bad things happened while initializing EnvironmentGenerator.");
}

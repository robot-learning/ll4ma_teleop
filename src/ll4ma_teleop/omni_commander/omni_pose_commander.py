#!/usr/bin/env python
import sys
import rospy
import numpy as np
import tf
from copy import deepcopy
from geometry_msgs.msg import WrenchStamped, Pose, Point, Quaternion
from phantom_omni.msg import OmniFeedback
from ll4ma_teleop.msg import ControlBoardState, OmniCommand
from std_srvs.srv import SetBool, SetBoolResponse


class OmniPoseCommander:
    def __init__(self):
        self.tf_listener = tf.TransformListener()
        self.omni_pose = np.array([0, 0, 0, 0, 0, 0, 1])
        self.prev_omni_pose = np.array([0, 0, 0, 0, 0, 0, 1])
        self.pose_diff = np.zeros(6)
        self.factor = np.zeros(6)  # Set by client control panel
        self.robot_force = np.zeros(3)

        ft_topic = rospy.get_param("~ft_topic")
        omni_fb_topic = rospy.get_param("~omni_fb_topic")
        pose_cmd_topic = rospy.get_param("~pose_cmd_topic")
        rate_val = rospy.get_param("~rate")

        self.rate = rospy.Rate(rate_val)
        self.force_cmd_pub = rospy.Publisher(
            omni_fb_topic, OmniFeedback, queue_size=1)
        self.pose_cmd_pub = rospy.Publisher(
            pose_cmd_topic, OmniCommand, queue_size=1)
        rospy.Subscriber(ft_topic, WrenchStamped, self._robot_force_cb)

        # Services this node offers
        force_srv = rospy.Service("/omni_commander/enable_force_feedback",
                                  SetBool, self._set_force_fb_active)
        pos_srv = rospy.Service("/omni_commander/enable_position", SetBool,
                                self._set_position_active)
        orient_srv = rospy.Service("/omni_commander/enable_orientation",
                                   SetBool, self._set_orientation_active)

        self.force_fb_active = False

    def run(self):
        rospy.loginfo("Waiting for Omni state...")
        while not rospy.is_shutdown() and self.omni_pose is None:
            self.omni_pose = self._get_omni_pose()
            self.prev_omni_pose = deepcopy(self.omni_pose)
            self.rate.sleep()
        rospy.loginfo("Omni state received.")
        rospy.loginfo("Commanding relative pose difference...")
        while not rospy.is_shutdown():
            self._command()
            self.rate.sleep()

    def _command(self):
        if self.force_fb_active:
            self._command_force()

        omni_pose = self._get_omni_pose()
        # update if it actually returned something
        if omni_pose is not None:
            self.omni_pose = omni_pose

        cmd = OmniCommand()
        cmd.pose = Pose(
            Point(*self.omni_pose[:3]), Quaternion(*self.omni_pose[3:]))
        cmd.factor = self.factor
        self.pose_cmd_pub.publish(cmd)

    def _command_force(self, limit=1.0, max_robot_force=1.0):
        cmd = OmniFeedback()
        # scale to be relative to maximum force
        force = self.robot_force * (limit / max_robot_force)
        cmd.force.x = np.sign(force[0]) * min(abs(force[0]), limit)
        cmd.force.y = np.sign(force[1]) * min(abs(force[1]), limit)
        cmd.force.z = np.sign(force[2]) * min(abs(force[2]), limit)
        self.force_cmd_pub.publish(cmd)

    def _set_force_fb_active(self, req):
        self.force_fb_active = req.data
        if req.data:
            rospy.loginfo("Force feedback enabled.")
        else:
            rospy.loginfo("Force feedback disabled.")
        return SetBoolResponse(success=True)

    def _set_position_active(self, req):
        # TODO make custom srv and set floats for scale factor
        if req.data:
            self.factor[:3] = np.ones(3)
            rospy.loginfo("Position enabled.")
        else:
            self.factor[:3] = np.zeros(3)
            rospy.loginfo("Position Disabled.")
        return SetBoolResponse(success=True)

    def _set_orientation_active(self, req):
        if req.data:
            self.factor[3:] = np.ones(3)
            rospy.loginfo("Position enabled.")
        else:
            self.factor[3:] = np.zeros(3)
            rospy.loginfo("Position disabled.")
        return SetBoolResponse(success=True)

    def _robot_force_cb(self, wrench_stmp):
        f = wrench_stmp.wrench.force
        self.robot_force[0] = f.x
        self.robot_force[1] = f.y
        self.robot_force[2] = f.z

    def _get_omni_pose(self):
        omni_trans = None
        omni_rot = None
        pose = None
        try:
            omni_trans, omni_rot = self.tf_listener.lookupTransform(
                "world", "endpoint", rospy.Time(0))
        except (tf.LookupException, tf.ConnectivityException,
                tf.ExtrapolationException) as e:
            pass
        if omni_trans is not None and omni_rot is not None:
            omni_trans = np.array(omni_trans)
            omni_rot = np.array(omni_rot)
            pose = np.hstack((omni_trans, omni_rot))
        return pose


if __name__ == '__main__':
    rospy.init_node('omni_pose_commander')
    commander = OmniPoseCommander()
    try:
        commander.run()
    except rospy.ROSInterruptException:
        pass

#!/usr/bin/env python
import os
import sys
import errno
import rospy
import Tkinter as tk
import tkMessageBox
import ttk
import tkFont
from ll4ma_teleop.msg import ControlBoardState
from geometry_msgs.msg import Wrench, WrenchStamped
from std_srvs.srv import SetBool, SetBoolRequest, Trigger, TriggerResponse


class OmniControlBoard:
    def __init__(self):
        cb_topic = rospy.get_param("~omni_cb_topic")
        self.state_pub = rospy.Publisher(
            cb_topic, ControlBoardState, queue_size=1)
        self.state = ControlBoardState()
        self.rot_state = 0.0
        self.recording = False
        self.demo_names = []
        self._create_board()

    def run(self):
        self.root.mainloop()

    def _slide_cb(self, val):
        # position sliders
        if self.pos_x_slide.get() == 0.0:
            self.pos_x_btn_txt.set("Enable")
        else:
            self.pos_x_val = self.pos_x_slide.get()
            self.pos_x_btn_txt.set("Disable")
        if self.pos_y_slide.get() == 0.0:
            self.pos_y_btn_txt.set("Enable")
        else:
            self.pos_y_val = self.pos_y_slide.get()
            self.pos_y_btn_txt.set("Disable")
        if self.pos_z_slide.get() == 0.0:
            self.pos_z_btn_txt.set("Enable")
        else:
            self.pos_z_val = self.pos_z_slide.get()
            self.pos_z_btn_txt.set("Disable")
        # rotation sliders
        if self.rot_x_slide.get() == 0.0:
            self.rot_x_btn_txt.set("Enable")
        else:
            self.rot_x_val = self.rot_x_slide.get()
            self.rot_x_btn_txt.set("Disable")
        if self.rot_y_slide.get() == 0.0:
            self.rot_y_btn_txt.set("Enable")
        else:
            self.rot_y_val = self.rot_y_slide.get()
            self.rot_y_btn_txt.set("Disable")
        if self.rot_z_slide.get() == 0.0:
            self.rot_z_btn_txt.set("Enable")
        else:
            self.rot_z_val = self.rot_z_slide.get()
            self.rot_z_btn_txt.set("Disable")
        self.state.slider_vals = [
            self.pos_x_slide.get(),
            self.pos_y_slide.get(),
            self.pos_z_slide.get(),
            self.rot_x_slide.get(),
            self.rot_y_slide.get(),
            self.rot_z_slide.get()
        ]
        self.state_pub.publish(self.state)

    def _pos_x_btn_cb(self):
        if self.pos_x_btn_txt.get() == "Disable":
            self.pos_x_val = self.pos_x_slide.get()
            self.pos_x_slide.set(0.0)
        else:
            self.pos_x_slide.set(self.pos_x_val)

    def _pos_y_btn_cb(self):
        if self.pos_y_btn_txt.get() == "Disable":
            self.pos_y_val = self.pos_y_slide.get()
            self.pos_y_slide.set(0.0)
        else:
            self.pos_y_slide.set(self.pos_y_val)

    def _pos_z_btn_cb(self):
        if self.pos_z_btn_txt.get() == "Disable":
            self.pos_z_val = self.pos_z_slide.get()
            self.pos_z_slide.set(0.0)
        else:
            self.pos_z_slide.set(self.pos_z_val)

    def _all_pos_btn_cb(self):
        if self.all_pos_btn_txt.get() == "Disengage Position":
            self.pos_x_val = self.pos_x_slide.get()
            self.pos_y_val = self.pos_y_slide.get()
            self.pos_z_val = self.pos_z_slide.get()
            self.pos_x_slide.set(0.0)
            self.pos_y_slide.set(0.0)
            self.pos_z_slide.set(0.0)
            self.all_pos_btn_txt.set("Engage Position")
        else:
            self.pos_x_slide.set(self.pos_x_val)
            self.pos_y_slide.set(self.pos_y_val)
            self.pos_z_slide.set(self.pos_z_val)
            self.all_pos_btn_txt.set("Disengage Position")

    def _rot_x_btn_cb(self):
        if self.rot_x_btn_txt.get() == "Disable":
            self.rot_x_val = self.rot_x_slide.get()
            self.rot_x_slide.set(0.0)
        else:
            self.rot_x_slide.set(self.rot_x_val)

    def _rot_y_btn_cb(self):
        if self.rot_y_btn_txt.get() == "Disable":
            self.rot_y_val = self.rot_y_slide.get()
            self.rot_y_slide.set(0.0)
        else:
            self.rot_y_slide.set(self.rot_y_val)

    def _rot_z_btn_cb(self):
        if self.rot_z_btn_txt.get() == "Disable":
            self.rot_z_val = self.rot_z_slide.get()
            self.rot_z_slide.set(0.0)
        else:
            self.rot_z_slide.set(self.rot_z_val)

    def _all_rot_btn_cb(self):
        if self.all_rot_btn_txt.get() == "Disengage Orientation":
            self.rot_x_val = self.rot_x_slide.get()
            self.rot_y_val = self.rot_y_slide.get()
            self.rot_z_val = self.rot_z_slide.get()
            self.rot_x_slide.set(0.0)
            self.rot_y_slide.set(0.0)
            self.rot_z_slide.set(0.0)
            self.all_rot_btn_txt.set("Engage Orientation")
        else:
            self.rot_x_slide.set(self.rot_x_val)
            self.rot_y_slide.set(self.rot_y_val)
            self.rot_z_slide.set(self.rot_z_val)
            self.all_rot_btn_txt.set("Disengage Orientation")

    def _force_btn_cb(self):
        if self.force_btn_txt.get() == "Disable Force Feedback":
            self.state.force_fb_active = False
            self.force_btn_txt.set("Enable Force Feedback")
        else:
            self.state.force_fb_active = True
            self.force_btn_txt.set("Disable Force Feedback")
        self.state_pub.publish(self.state)

    def _reset_btn_cb(self):
        try:
            reset_robot = rospy.ServiceProxy(
                "/robot_commander/reset_robot_state", Trigger)
            reset_robot()
        except rospy.ServiceException:
            pass
        try:
            reset = rospy.ServiceProxy("dart_environment/reset_environment",
                                       Trigger)
            reset()
        except rospy.ServiceException:
            pass

    def _sync_state_btn_cb(self):
        pass

    def _record_btn_cb(self):
        pass

    def _execute_sim_btn_cb(self):
        pass

    def _execute_robot_btn_cb(self):
        pass

    def _dmp_btn_cb(self):
        self.dmp_btn.config(relief=tk.SUNKEN)
        self.promp_btn.config(relief=tk.RAISED)
        self.policy_type = "dmp"

    def _promp_btn_cb(self):
        self.promp_btn.config(relief=tk.SUNKEN)
        self.dmp_btn.config(relief=tk.RAISED)
        self.policy_type = "promp"

    def _jnt_btn_cb(self):
        pass

    def _task_btn_cb(self):
        pass

    def _create_board(self):
        self.root = tk.Tk()  # create a Tk root window
        self.root.title("Omni Control Board")
        w = 840
        h = 700
        ws = self.root.winfo_screenwidth()
        hs = self.root.winfo_screenheight()
        x = (ws / 2) - (w / 2) - 400
        y = (hs / 2) - (h / 2)
        self.root.geometry('%dx%d+%d+%d' % (w, h, x, y))
        btn_font = tkFont.Font(family='Helvetica', size=11, weight='bold')

        omni_frame = tk.Frame(self.root)
        trajectory_frame = tk.Frame(self.root, bd=20)

        slider_frame = tk.Frame(omni_frame, bd=10)

        left_slider_frame = tk.Frame(
            slider_frame,
            highlightthickness=3,
            highlightbackground="gray40",
            bd=10)
        pos_slider_frame = tk.Frame(left_slider_frame)
        pos_btn_frame = tk.Frame(left_slider_frame)
        all_pos_btn_frame = tk.Frame(left_slider_frame)

        right_slider_frame = tk.Frame(
            slider_frame,
            highlightthickness=3,
            highlightbackground="gray40",
            bd=10)
        rot_slider_frame = tk.Frame(right_slider_frame)
        rot_btn_frame = tk.Frame(right_slider_frame)
        all_rot_btn_frame = tk.Frame(right_slider_frame)

        empty_frame = tk.Frame(slider_frame)

        force_frame = tk.Frame(omni_frame, bd=10)
        force_btn_frame = tk.Frame(
            force_frame,
            highlightthickness=5,
            highlightbackground="DodgerBlue2",
            bd=5)

        policy_btn_frame = tk.Frame(trajectory_frame, bd=10)
        control_btn_frame = tk.Frame(trajectory_frame, bd=10)
        traj_btn_frame = tk.Frame(
            trajectory_frame,
            highlightthickness=5,
            highlightbackground="SpringGreen3",
            bd=10)
        demo_frame = tk.Frame(trajectory_frame, bd=10)

        self.pos_x_slide = tk.Scale(
            pos_slider_frame,
            from_=5,
            to_=0,
            label='X',
            command=self._slide_cb,
            length=400,
            width=30,
            resolution=0.1,
            bd=3)
        self.pos_y_slide = tk.Scale(
            pos_slider_frame,
            from_=5,
            to_=0,
            label='Y',
            command=self._slide_cb,
            length=400,
            width=30,
            resolution=0.1,
            bd=3)
        self.pos_z_slide = tk.Scale(
            pos_slider_frame,
            from_=5,
            to_=0,
            label='Z',
            command=self._slide_cb,
            length=400,
            width=30,
            resolution=0.1,
            bd=3)
        self.rot_x_slide = tk.Scale(
            rot_slider_frame,
            from_=2,
            to_=0,
            label='X',
            command=self._slide_cb,
            length=400,
            width=30,
            resolution=0.1,
            bd=3)
        self.rot_y_slide = tk.Scale(
            rot_slider_frame,
            from_=2,
            to_=0,
            label='Y',
            command=self._slide_cb,
            length=400,
            width=30,
            resolution=0.1,
            bd=3)
        self.rot_z_slide = tk.Scale(
            rot_slider_frame,
            from_=2,
            to_=0,
            label='Z',
            command=self._slide_cb,
            length=400,
            width=30,
            resolution=0.1,
            bd=3)
        self.pos_x_val = 1.0
        self.pos_y_val = 1.0
        self.pos_z_val = 1.0
        self.rot_x_val = 1.5
        self.rot_y_val = 1.5
        self.rot_z_val = 1.5
        self.pos_x_slide.set(0.0)
        self.pos_y_slide.set(0.0)
        self.pos_z_slide.set(0.0)
        self.rot_x_slide.set(0.0)
        self.rot_y_slide.set(0.0)
        self.rot_z_slide.set(0.0)
        self.pos_x_slide.pack(side=tk.LEFT)
        self.pos_y_slide.pack(side=tk.LEFT)
        self.pos_z_slide.pack(side=tk.LEFT)
        self.rot_x_slide.pack(side=tk.LEFT)
        self.rot_y_slide.pack(side=tk.LEFT)
        self.rot_z_slide.pack(side=tk.LEFT)

        self.pos_x_btn_txt = tk.StringVar()
        self.pos_y_btn_txt = tk.StringVar()
        self.pos_z_btn_txt = tk.StringVar()
        self.rot_x_btn_txt = tk.StringVar()
        self.rot_y_btn_txt = tk.StringVar()
        self.rot_z_btn_txt = tk.StringVar()

        self.pos_x_btn = tk.Button(
            pos_btn_frame,
            textvariable=self.pos_x_btn_txt,
            command=self._pos_x_btn_cb,
            width=7,
            bd=3,
            pady=25)
        self.pos_y_btn = tk.Button(
            pos_btn_frame,
            textvariable=self.pos_y_btn_txt,
            command=self._pos_y_btn_cb,
            width=7,
            bd=3,
            pady=25)
        self.pos_z_btn = tk.Button(
            pos_btn_frame,
            textvariable=self.pos_z_btn_txt,
            command=self._pos_z_btn_cb,
            width=7,
            bd=3,
            pady=25)
        self.rot_x_btn = tk.Button(
            rot_btn_frame,
            textvariable=self.rot_x_btn_txt,
            command=self._rot_x_btn_cb,
            width=7,
            bd=3,
            pady=25)
        self.rot_y_btn = tk.Button(
            rot_btn_frame,
            textvariable=self.rot_y_btn_txt,
            command=self._rot_y_btn_cb,
            width=7,
            bd=3,
            pady=25)
        self.rot_z_btn = tk.Button(
            rot_btn_frame,
            textvariable=self.rot_z_btn_txt,
            command=self._rot_z_btn_cb,
            width=7,
            bd=3,
            pady=25)

        self.pos_x_btn_txt.set("Enable")
        self.pos_y_btn_txt.set("Enable")
        self.pos_z_btn_txt.set("Enable")
        self.rot_x_btn_txt.set("Enable")
        self.rot_y_btn_txt.set("Enable")
        self.rot_z_btn_txt.set("Enable")
        self.pos_x_btn.pack(side=tk.LEFT)
        self.pos_y_btn.pack(side=tk.LEFT)
        self.pos_z_btn.pack(side=tk.LEFT)
        self.rot_x_btn.pack(side=tk.LEFT)
        self.rot_y_btn.pack(side=tk.LEFT)
        self.rot_z_btn.pack(side=tk.LEFT)

        self.all_pos_btn_txt = tk.StringVar()
        self.all_rot_btn_txt = tk.StringVar()
        self.all_pos_btn = tk.Button(
            all_pos_btn_frame,
            textvariable=self.all_pos_btn_txt,
            command=self._all_pos_btn_cb,
            width=29,
            bd=3,
            pady=25)
        self.all_rot_btn = tk.Button(
            all_rot_btn_frame,
            textvariable=self.all_rot_btn_txt,
            command=self._all_rot_btn_cb,
            width=29,
            bd=3,
            pady=25)
        self.all_pos_btn_txt.set("Engage Position")
        self.all_rot_btn_txt.set("Engage Orientation")
        self.all_pos_btn.pack()
        self.all_rot_btn.pack()

        self.force_btn_txt = tk.StringVar()
        self.force_btn = tk.Button(
            force_btn_frame,
            textvariable=self.force_btn_txt,
            command=self._force_btn_cb,
            width=66,
            bd=3,
            pady=25)
        self.force_btn_txt.set("Enable Force Feedback")
        self.force_btn.pack()

        self.reset_btn_txt = tk.StringVar()
        self.reset_btn = tk.Button(
            traj_btn_frame,
            textvariable=self.reset_btn_txt,
            command=self._reset_btn_cb,
            width=15,
            bd=3,
            pady=10)
        self.reset_btn_txt.set("Reset\nEnvironment")
        self.reset_btn.pack()

        self.sync_state_btn_txt = tk.StringVar()
        self.sync_state_btn = tk.Button(
            traj_btn_frame,
            textvariable=self.sync_state_btn_txt,
            command=self._sync_state_btn_cb,
            width=15,
            bd=3,
            pady=10)
        self.sync_state_btn_txt.set("Sync With Remote\nRobot State")
        self.sync_state_btn.pack()

        self.record_btn_txt = tk.StringVar()
        self.record_btn = tk.Button(
            traj_btn_frame,
            textvariable=self.record_btn_txt,
            command=self._record_btn_cb,
            width=15,
            bd=3,
            pady=10)
        self.record_btn_txt.set("Record\nDemonstration")
        self.record_btn.pack()

        self.execute_sim_btn_txt = tk.StringVar()
        self.execute_sim_btn = tk.Button(
            traj_btn_frame,
            textvariable=self.execute_sim_btn_txt,
            command=self._execute_sim_btn_cb,
            width=15,
            bd=3,
            pady=10)
        self.execute_sim_btn_txt.set("Execute In\nSimulator")
        self.execute_sim_btn.pack()

        self.execute_robot_btn_txt = tk.StringVar()
        self.execute_robot_btn = tk.Button(
            traj_btn_frame,
            textvariable=self.execute_robot_btn_txt,
            command=self._execute_robot_btn_cb,
            width=15,
            bd=3,
            pady=10)
        self.execute_robot_btn_txt.set("Execute On\nRobot")
        self.execute_robot_btn.pack()

        self.reset_btn['font'] = btn_font
        self.sync_state_btn['font'] = btn_font
        self.record_btn['font'] = btn_font
        self.execute_sim_btn['font'] = btn_font
        self.execute_robot_btn['font'] = btn_font

        self.dmp_btn_txt = tk.StringVar()
        self.dmp_btn = tk.Button(
            policy_btn_frame,
            textvariable=self.dmp_btn_txt,
            command=self._dmp_btn_cb,
            width=7,
            bd=3,
            pady=10)
        self.dmp_btn_txt.set("DMP")
        self.dmp_btn.pack(side=tk.LEFT)
        self.promp_btn_txt = tk.StringVar()
        self.promp_btn = tk.Button(
            policy_btn_frame,
            textvariable=self.promp_btn_txt,
            command=self._promp_btn_cb,
            width=7,
            bd=3,
            pady=10)
        self.promp_btn_txt.set("ProMP")
        self.promp_btn.pack(side=tk.LEFT)

        self.jnt_btn_txt = tk.StringVar()
        self.jnt_btn = tk.Button(
            control_btn_frame,
            textvariable=self.jnt_btn_txt,
            command=self._jnt_btn_cb,
            width=7,
            bd=3,
            pady=10)
        self.jnt_btn_txt.set("Joint")
        self.jnt_btn.pack(side=tk.LEFT)

        self.task_btn_txt = tk.StringVar()
        self.task_btn = tk.Button(
            control_btn_frame,
            textvariable=self.task_btn_txt,
            command=self._task_btn_cb,
            width=7,
            bd=3,
            pady=10)
        self.task_btn_txt.set("Task")
        self.task_btn.pack(side=tk.LEFT)

        self.demo_counter = tk.Label(demo_frame)
        self.demo_counter.config(text="Number of Demos: %d" % 0)
        self.demo_counter.config(font=btn_font)
        self.demo_counter.pack()

        # default to promp
        self._promp_btn_cb()
        # self._dmp_btn_cb()
        # default to joint space control
        # self._jnt_btn_cb()
        self._task_btn_cb()
        self._reset_btn_cb()

        pos_slider_frame.pack()
        pos_btn_frame.pack()
        all_pos_btn_frame.pack()
        left_slider_frame.pack(side=tk.LEFT)

        empty_frame.pack(
            side=tk.LEFT,
            ipadx=4)  # kind of a hack to get space between frames

        rot_slider_frame.pack()
        rot_btn_frame.pack()
        all_rot_btn_frame.pack()
        right_slider_frame.pack(side=tk.LEFT)

        force_btn_frame.pack()

        slider_frame.pack(side=tk.TOP)
        force_frame.pack(side=tk.BOTTOM)

        omni_frame.pack(side=tk.LEFT)

        policy_btn_frame.pack(side=tk.TOP)
        control_btn_frame.pack(side=tk.TOP)
        traj_btn_frame.pack(side=tk.TOP)
        demo_frame.pack(side=tk.TOP)
        trajectory_frame.pack(side=tk.LEFT)


if __name__ == '__main__':
    rospy.init_node("omni_control_board")
    argv = rospy.myargv(argv=sys.argv)
    ocb = OmniControlBoard(*argv[1:]) if len(argv) > 1 else OmniControlBoard()
    ocb.run()

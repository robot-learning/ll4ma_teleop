#include <ll4ma_teleop/robot_commander/joint_commander.h>
#include <kdl_conversions/kdl_msg.h>
#include <kdl/chainfksolverpos_recursive.hpp>
#include <kdl/chainfksolvervel_recursive.hpp>


bool RobotJointCommander::init()
{
  ROS_INFO("Initializing...");

  bool success = true;
  success &= nh_.getParam("teleop_cmd_topic", teleop_cmd_topic_);
  success &= nh_.getParam("jnt_cmd_topic", jnt_cmd_topic_);
  success &= nh_.getParam("jnt_state_topic", jnt_state_topic_);
  success &= nh_.getParam("root_link", root_link_);
  success &= nh_.getParam("tip_link", tip_link_);
  success &= nh_.getParam("/joint_upper_limits", jnt_upper_lims_);
  success &= nh_.getParam("/joint_lower_limits", jnt_lower_lims_);
  success &= nh_.getParam("/robot_description", robot_description_);
  success &= nh_.param<bool>("use_current_joint_state", use_current_joint_state_, false);
  if (!use_current_joint_state_)
    success &= nh_.getParam("/nominal_angles", nominal_jnts_);
  if (!success)
  {
    ROS_ERROR("Failed to load params from param server.");
    return false;
  }

  // Initialize the KDL chain
  if (!kdl_parser::treeFromString(robot_description_, robot_tree_))
  {
    ROS_ERROR("Failed to construct kdl tree.");
    return false;
  }
  if (!robot_tree_.getChain(root_link_, tip_link_, kdl_chain_))
  {
    ROS_ERROR("Failed to construct kdl chain.");
    return false;
  }
  num_jnts_ = kdl_chain_.getNrOfJoints();
  KDL::Joint::JointType revolute = KDL::Joint::None;
  for (int i = 0; i < kdl_chain_.getNrOfSegments(); i++)
    if (kdl_chain_.getSegment(i).getJoint().getType() != revolute)
      jnt_names_.push_back(kdl_chain_.getSegment(i).getJoint().getName());

  fk_solver_.reset(new KDL::ChainFkSolverVel_recursive(kdl_chain_));  

  // Initialize kinematic components
  J_kdl_.resize(num_jnts_);
  q_qdot_.resize(num_jnts_);
  KDL::SetToZero(q_qdot_);
  prev_q_.resize(num_jnts_);
  x_diff_.setZero(6);
  factor_.setZero(6);
  q_diff_.setZero(num_jnts_);
  J_.setZero(6, num_jnts_);
  J_inv_.setZero(num_jnts_, 6);

  jac_solver_.reset(new KDL::ChainJntToJacSolver(kdl_chain_));

  // Initialize ROS
  jnt_cmd_pub_ = nh_.advertise<sensor_msgs::JointState>(jnt_cmd_topic_, 1);
  teleop_cmd_sub_ = nh_.subscribe(teleop_cmd_topic_, 1, &RobotJointCommander::teleopCommandCallback,
                                  this);
  jnt_state_sub_ = nh_.subscribe(jnt_state_topic_, 1, &RobotJointCommander::jointStateCallback, this);
  reset_srv_ = nh_.advertiseService("/robot_commander/reset_robot_state",
                                    &RobotJointCommander::resetRobotState, this);
  set_nominal_srv_ = nh_.advertiseService("/robot_commander/set_nominal_robot_state",
                                          &RobotJointCommander::setNominalRobotState, this);
  initJointCommand();

  ROS_INFO("Initialization complete.");
  return true;
}


void RobotJointCommander::run()
{
  ROS_INFO("Ready to command joint positions.");
  while (ros::ok())
  {
    if (controlEngaged() and have_cmd_)
    {
      if (use_current_joint_state_)
      {
        // Update live from the real joint state (i.e. assume we're commanding 'real' system)
        for (int i = 0; i < num_jnts_; ++i)
          q_qdot_.q(i) = jnt_state_.position[i];
        fk_solver_->JntToCart(q_qdot_, x_xdot_);
      }
        
      jac_solver_->JntToJac(q_qdot_.q, J_kdl_);
      J_ = J_kdl_.data;
      getPseudoInverse(J_, J_inv_, 1.e-5);

      x_diff_kdl_ = KDL::diff(prev_x_, x_); // current - previous
      for (std::size_t i = 0; i < 6; ++i)
        x_diff_[i] = factor_[i] * x_diff_kdl_[i];

      q_diff_ = J_inv_ * x_diff_;

      // Update joint angles
      for (std::size_t i = 0; i < num_jnts_; ++i)
      {
        q_qdot_.q(i) += q_diff_[i];
        // Threshold to limits
        q_qdot_.q(i) = std::max(q_qdot_.q(i), jnt_lower_lims_[i]);
        q_qdot_.q(i) = std::min(q_qdot_.q(i), jnt_upper_lims_[i]);
        // Update joint velocity
        q_qdot_.qdot(i) = q_diff_[i] / dt_;
      }

      for (std::size_t i = 0; i < num_jnts_; ++i)
      {
        jnt_cmd_.position[i] = q_qdot_.q(i);
        jnt_cmd_.velocity[i] = q_qdot_.qdot(i);
      }

      jnt_cmd_pub_.publish(jnt_cmd_);
      have_cmd_ = false;
    }
    
    ros::spinOnce();
    rate_.sleep();
  }
  ROS_INFO("Done commanding. Exiting");
}


void RobotJointCommander::teleopCommandCallback(ll4ma_teleop::TeleopCommand cmd)
{
  prev_x_ = x_;
  tf::poseMsgToKDL(cmd.pose, x_);
  if (cmd.factor.size() == factor_.size())
    for (std::size_t i = 0; i < factor_.size(); ++i)
      factor_[i] = cmd.factor[i];
  have_cmd_ = true;
}


void RobotJointCommander::getPseudoInverse(Eigen::MatrixXd &m, Eigen::MatrixXd &m_pinv,
                                           double tolerance)
{
  Eigen::JacobiSVD<Eigen::MatrixXd> svd(m, Eigen::ComputeFullU | Eigen::ComputeFullV);
  Eigen::JacobiSVD<Eigen::MatrixXd>::SingularValuesType sing_vals = svd.singularValues();
  // set values within tolerance to zero
  for (std::size_t idx = 0; idx < sing_vals.size(); idx++)
  {
    if (tolerance > 0.0 && sing_vals(idx) > tolerance)
      sing_vals(idx) = 1.0 / sing_vals(idx);
    else
      sing_vals(idx) = 0.0;
  }

  m_pinv = svd.matrixV().leftCols(sing_vals.size()) * sing_vals.asDiagonal() * svd.matrixU().leftCols(sing_vals.size()).transpose();
}


void RobotJointCommander::initJointCommand()
{
  if (use_current_joint_state_)
  {
    ROS_INFO("Waiting for current joint state...");
    while (ros::ok() and jnt_state_.position.size() == 0)
    {
      ros::spinOnce();
      rate_.sleep();
    }
    ROS_INFO("Joint state received!");
    for (int i = 0; i < num_jnts_; ++i)
      nominal_jnts_.push_back(jnt_state_.position[i]);
  }

  ROS_INFO("Initializing joint command...");
  for (int i = 0; i < num_jnts_; ++i)
  {
    jnt_cmd_.name.push_back(jnt_names_[i]);
    jnt_cmd_.position.push_back(nominal_jnts_[i]);
    jnt_cmd_.velocity.push_back(0.0);
    jnt_cmd_.effort.push_back(0.0);
    q_qdot_.q(i) = nominal_jnts_[i];
  }
  
  ROS_INFO("Joint command initialization complete.");
}


bool RobotJointCommander::resetRobotState(std_srvs::Trigger::Request &req,
                                          std_srvs::Trigger::Response &resp)
{
  ROS_INFO("Robot state reset to nominal state.");
  for (int i = 0; i < num_jnts_; ++i)
    q_qdot_.q(i) = nominal_jnts_[i];
  resp.success = true;
  return true;
}


bool RobotJointCommander::controlEngaged()
{
  double sum = 0.0;
  for (std::size_t i = 0; i < factor_.size(); ++i)
    sum += factor_[i];

  return sum > 0.0;
}


bool RobotJointCommander::setNominalRobotState(std_srvs::Trigger::Request &req,
                                               std_srvs::Trigger::Response &resp)
{
  ROS_INFO("Nominal robot state set to current robot state.");
  for (std::size_t i = 0; i < num_jnts_; ++i)
    nominal_jnts_[i] = q_qdot_.q(i);
  resp.success = true;
  return true;
}


void RobotJointCommander::jointStateCallback(sensor_msgs::JointState msg)
{
  jnt_state_ = msg;
}


int main(int argc, char **argv)
{
  ros::init(argc, argv, "robot_joint_commander");
  ros::NodeHandle nh("~");
  int rate;
  nh.getParam("rate", rate);
  RobotJointCommander commander(rate, nh);
  if (commander.init())
    commander.run();
  else
    ROS_ERROR("Bad things happened in initializing RobotJointCommander");
}

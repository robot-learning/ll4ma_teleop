#!/usr/bin/env python
import sys
import rospy
from ll4ma_policy_learning.srv import PolicyExecution, PolicyExecutionRequest



if __name__ == '__main__':
    rospy.init_node("batch_policy_commander")
    
    argv = rospy.myargv(argv=sys.argv)

    if len(argv) > 1:
        # TODO hardcoding a lot just to get this running quickly
        demo_filename = "demos_%d.log" % int(argv[1])
        teleop_robot_name = "lbr4_teleop"
        policy_type = "promp"
        task_space = True
        num_executions = 100
        
        # command policy
        try:
            command = rospy.ServiceProxy("/%s/policy_commander/command_policy"
                                         % teleop_robot_name, PolicyExecution)
            req = PolicyExecutionRequest()
            req.path = "~/.ros/ll4ma_teleop"
            req.filename = demo_filename
            req.policy_type = policy_type
            req.num_executions = num_executions
            req.is_joint_space = True
            resp = command(req)
        except rospy.ServiceException, e:
            rospy.logwarn("Policy execution request failed: %s" % e)
    else:
        rospy.logerr("Must pass index of demo file.")

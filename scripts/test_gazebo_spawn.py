#!/usr/bin/env python
import sys
import rospy
from time import time
from geometry_msgs.msg import Pose
from gazebo_msgs.srv import SpawnModel
import numpy as np

rospy.init_node("test_gazebo_spawn")

if not rospy.has_param("bowl_description"):
    rospy.logwarn("Description not loaded")
    sys.exit(1)
description = rospy.get_param("bowl_description")
init_pose = Pose()
init_pose.position.x = np.random.uniform(-0.05, 0.05)
init_pose.position.y = np.random.uniform(-0.15, 0.15)
init_pose.position.z = 2.0


try:
    spawn = rospy.ServiceProxy("/gazebo/spawn_urdf_model", SpawnModel)
    spawn(model_name="bowl_{}".format(int(time())), model_xml=description, initial_pose=init_pose)
except rospy.ServiceException:
    rospy.logwarn("Service call failed")

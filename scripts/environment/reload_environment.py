#!/usr/bin/env python

import rospy
from std_srvs.srv import Empty

if __name__ == '__main__':
    rospy.init_node('env_reloader')

    try:
        reset = rospy.ServiceProxy("environment/reset", Empty)
        success = reset()
    except rospy.ServiceException, e:
        rospy.logerr("Service call to reset environment failed.")

    if success:
        rospy.loginfo("Service call to reset environment was successful!")
